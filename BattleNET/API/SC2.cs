﻿using BattleNET.Objects.SC2;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.API
{
    public class SC2
    {
        private BattleNETClient parent;

        private string api_url;
        private string locale;
        private string api_key;
        private string user_agent;

        public SC2(BattleNETClient parent, string api_url, string locale, string api_key, string user_agent)
        {
            this.parent = parent;

            this.api_url = api_url;
            this.locale = locale;
            this.api_key = api_key;
            this.user_agent = user_agent;
        }

        #region PROFILE API
        public Profile getProfile(int id, int region, string name)
        {
            Request request = new Request(user_agent);
            request.GET($"{api_url}sc2/profile/{id}/{region}/{name}/?locale={locale}&apikey={api_key}");

            return new Profile(JObject.Parse(request.response));
        }
        public Profile.ProfileLadders getLadders(int id, int region, string name)
        {
            Request request = new Request(user_agent);
            request.GET($"{api_url}sc2/profile/{id}/{region}/{name}/ladders?locale={locale}&apikey={api_key}");

            return new Profile.ProfileLadders(JObject.Parse(request.response));
        }
        public List<Match> getMatchHistory(int id, int region, string name)
        {
            Request request = new Request(user_agent);
            request.GET($"{api_url}sc2/profile/{id}/{region}/{name}/matches?locale={locale}&apikey={api_key}");

            List<Match> matches = new List<Match>();
            foreach (JObject JMatch in JObject.Parse(request.response)["matches"])
                matches.Add(new Match(JMatch));
            return matches;
        }
        #endregion

        #region LADDER API
        public Ladder getLadder(int id)
        {
            Request request = new Request(user_agent);
            request.GET($"{api_url}sc2/ladder/{id}?locale={locale}&apikey={api_key}");

            return new Ladder(JObject.Parse(request.response));
        }
        #endregion

        #region DATA RESOURCES
        public List<Achievement> getAchievements()
        {
            Request request = new Request(user_agent);
            request.GET($"{api_url}sc2/data/achievements?locale={locale}&apikey={api_key}");

            List<Achievement> achievements = new List<Achievement>();
            foreach (JObject JAchievement in JObject.Parse(request.response)["achievements"])
                achievements.Add(new Achievement(JAchievement));
            return achievements;
        }
        public List<Reward> getRewards()
        {
            Request request = new Request(user_agent);
            request.GET($"{api_url}sc2/data/rewards?locale={locale}&apikey={api_key}");

            List<Reward> rewards = new List<Reward>();
            foreach (JObject JReward in JObject.Parse(request.response)["rewards"])
                rewards.Add(new Reward(JReward));
            return rewards;
        }
        #endregion
    }
}
