﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.SC2
{
    public class Reward
    {
        public string title { get; internal set; }
        public int id { get; internal set; }
        public Icon icon { get; internal set; }
        public int achievementId { get; internal set; }

        public Reward(JObject rawJson)
        {
            this.title = rawJson["title"].ToString();
            this.id = int.Parse(rawJson["id"].ToString());
            this.icon = new Icon(JObject.Parse(rawJson["icon"].ToString()));
            this.achievementId = int.Parse(rawJson["achievementId"].ToString());
        }
    }
}
