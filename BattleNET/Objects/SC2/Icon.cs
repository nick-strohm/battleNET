﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.SC2
{
    public class Icon
    {
        public int x { get; internal set; }
        public int y { get; internal set; }
        public int w { get; internal set; }
        public int h { get; internal set; }
        public int offset { get; internal set; }
        public string url { get; internal set; }

        public Icon(JObject rawJson)
        {
            this.x = int.Parse(rawJson["x"].ToString());
            this.y = int.Parse(rawJson["y"].ToString());
            this.w = int.Parse(rawJson["w"].ToString());
            this.h = int.Parse(rawJson["h"].ToString());
            this.offset = int.Parse(rawJson["offset"].ToString());
            this.url = rawJson["url"].ToString();
        }
    }
}
