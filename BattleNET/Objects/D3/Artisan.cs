﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.D3
{
    public class Artisan
    {
        public class ArtisanTraining
        {
            public class ArtisanTrainingTier
            {
                public class ArtisanTrainingTierLevel
                {
                    public class ArtisanTrainingTierLevelRecipe
                    {
                        public class ArtisanTrainingTierLevelRecipeReagent
                        {
                            public int quantity { get; internal set; }
                            public Item item { get; internal set; }

                            public ArtisanTrainingTierLevelRecipeReagent(JObject JTrainingTierLevelRecipeReagent)
                            {
                                this.quantity = int.Parse(JTrainingTierLevelRecipeReagent["quantity"].ToString());
                                this.item = new Item(JObject.Parse(JTrainingTierLevelRecipeReagent["item"].ToString()));
                            }
                        }

                        public string id { get; internal set; }
                        public string slug { get; internal set; }
                        public string name { get; internal set; }
                        public int cost { get; internal set; }
                        public List<ArtisanTrainingTierLevelRecipeReagent> reagents { get; internal set; }
                        public Item itemProduced { get; internal set; }

                        public ArtisanTrainingTierLevelRecipe(JObject JTrainingTierLevelRecipe)
                        {
                            this.reagents = new List<ArtisanTrainingTierLevelRecipeReagent>();

                            this.id = JTrainingTierLevelRecipe["id"].ToString();
                            this.slug = JTrainingTierLevelRecipe["slug"].ToString();
                            this.name = JTrainingTierLevelRecipe["name"].ToString();
                            this.cost = int.Parse(JTrainingTierLevelRecipe["cost"].ToString());
                            foreach (JObject JReagent in JTrainingTierLevelRecipe["reagents"])
                                this.reagents.Add(new ArtisanTrainingTierLevelRecipeReagent(JReagent));
                            this.itemProduced = new Item(JObject.Parse(JTrainingTierLevelRecipe["itemProduced"].ToString()));
                        }
                    }

                    public int tier { get; internal set; }
                    public int tierLevel { get; internal set; }
                    public int percent { get; internal set; }
                    public List<ArtisanTrainingTierLevelRecipe> trainedRecipes { get; internal set; }
                    public List<ArtisanTrainingTierLevelRecipe> taughtRecipes { get; internal set; }
                    public int upgradeCost { get; internal set; }

                    public ArtisanTrainingTierLevel(JObject JTrainingTierLevel)
                    {
                        this.trainedRecipes = new List<ArtisanTrainingTierLevelRecipe>();
                        this.taughtRecipes = new List<ArtisanTrainingTierLevelRecipe>();

                        this.tier = int.Parse(JTrainingTierLevel["tier"].ToString());
                        this.tierLevel = int.Parse(JTrainingTierLevel["tierLevel"].ToString());
                        this.percent = int.Parse(JTrainingTierLevel["percent"].ToString());
                        foreach (JObject JTrainedRecipe in JTrainingTierLevel["trainedRecipes"])
                            this.trainedRecipes.Add(new ArtisanTrainingTierLevelRecipe(JTrainedRecipe));
                        foreach (JObject JTaughtRecipe in JTrainingTierLevel["taughtRecipes"])
                            this.taughtRecipes.Add(new ArtisanTrainingTierLevelRecipe(JTaughtRecipe));
                        this.upgradeCost = int.Parse(JTrainingTierLevel["upgradeCost"].ToString());
                    }
                }

                public int tier { get; internal set; }
                public List<ArtisanTrainingTierLevel> levels { get; internal set; }

                public ArtisanTrainingTier(JObject JTrainingTier)
                {
                    this.levels = new List<ArtisanTrainingTierLevel>();

                    this.tier = int.Parse(JTrainingTier["tier"].ToString());
                    foreach (JObject JLevel in JTrainingTier["levels"])
                        this.levels.Add(new ArtisanTrainingTierLevel(JLevel));
                }
            }

            public List<ArtisanTrainingTier> tiers { get; internal set; }

            public ArtisanTraining(JObject JTraining)
            {
                this.tiers = new List<ArtisanTrainingTier>();

                foreach (JObject JTier in JTraining["tiers"])
                    this.tiers.Add(new ArtisanTrainingTier(JTier));
            }
        }

        public string slug { get; internal set; }
        public string name { get; internal set; }
        public string portrait { get; internal set; }
        public ArtisanTraining training { get; internal set; }

        public Artisan(JObject rawJson)
        {
            this.slug = rawJson["slug"].ToString();
            this.name = rawJson["name"].ToString();
            this.portrait = rawJson["portrait"].ToString();
            this.training = new ArtisanTraining(JObject.Parse(rawJson["training"].ToString()));
        }
    }
}
