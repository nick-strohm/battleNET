﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.D3
{
    public class Item
    {
        public class ItemDyeColor
        {
            public Item item { get; internal set; }

            public ItemDyeColor(JObject JDyeColor)
            {
                this.item = new Item(JObject.Parse(JDyeColor["item"].ToString()));
            }
        }

        public string id { get; internal set; }
        public string name { get; internal set; }
        public string icon { get; internal set; }
        public string displayColor { get; internal set; }
        public string tooltipParams { get; internal set; }
        public ItemDyeColor dyeColor { get; internal set; }
        public List<string> setItemsEquipped { get; internal set; }

        public Item(JObject rawJson)
        {
            this.id = rawJson["id"].ToString();
            this.name = rawJson["name"].ToString();
            this.icon = rawJson["icon"].ToString();
            this.displayColor = rawJson["displayColor"].ToString();
            this.tooltipParams = rawJson["tooltipParams"].ToString();
            this.dyeColor = new ItemDyeColor(JObject.Parse(rawJson["dyeColor"].ToString()));
            this.setItemsEquipped = new List<string>();
            foreach (string JSetItemEquipped in rawJson["setItemsEquipped"])
                this.setItemsEquipped.Add(JSetItemEquipped);
        }
    }
}
