﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Mount
    {
        public string name { get; internal set; }
        public int spellId { get; internal set; }
        public int creatureId { get; internal set; }
        public int qualityId { get; internal set; }
        public string icon { get; internal set; }
        public bool isGround { get; internal set; }
        public bool isFlying { get; internal set; }
        public bool isAquatic { get; internal set; }
        public bool isJumping { get; internal set; }

        public Mount(JObject rawJson)
        {
            this.name = rawJson["name"].ToString();
            this.spellId = int.Parse(rawJson["spellId"].ToString());
            this.creatureId = int.Parse(rawJson["creatureId"].ToString());
            this.qualityId = int.Parse(rawJson["qualityId"].ToString());
            this.icon = rawJson["icon"].ToString();
            this.isGround = bool.Parse(rawJson["isGround"].ToString());
            this.isFlying = bool.Parse(rawJson["isFlying"].ToString());
            this.isAquatic = bool.Parse(rawJson["isAquatic"].ToString());
            this.isJumping = bool.Parse(rawJson["isJumping"].ToString());
        }
    }
}
