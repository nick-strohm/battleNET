﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class ItemSet
    {
        public class ItemSetSetBonus
        {
            public string description { get; internal set; }
            public int threshold { get; internal set; }

            public ItemSetSetBonus(JObject JSetBonuses)
            {
                this.description = JSetBonuses["description"].ToString();
                this.threshold = int.Parse(JSetBonuses["threshold"].ToString());
            }
        }

        public int id { get; internal set; }
        public string name { get; internal set; }
        public List<ItemSetSetBonus> setBonuses { get; internal set; }
        public List<int> items { get; internal set; }

        public ItemSet(JObject rawJson)
        {
            this.id = int.Parse(rawJson["id"].ToString());
            this.name = rawJson["name"].ToString();
            this.setBonuses = new List<ItemSetSetBonus>();
            foreach (JObject JSetBonus in rawJson["setBonuses"])
            {
                this.setBonuses.Add(new ItemSetSetBonus(JSetBonus));
            }
            this.items = new List<int>();
            foreach (int JItem in rawJson["items"])
            {
                this.items.Add(JItem);
            }
        }
    }
}
