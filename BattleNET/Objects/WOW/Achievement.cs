﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Achievement
    {
        public int id { get; internal set; }
        public string title { get; internal set; }
        public int points { get; internal set; }
        public string description { get; internal set; }
        public string reward { get; internal set; }
        public List<Item> rewardItems { get; internal set; }
        public string icon { get; internal set; }
        public List<Achievement> criteria { get; internal set; }
        public bool accountWide { get; internal set; }
        public int orderIndex { get; internal set; }
        public int max { get; internal set; }

        public Achievement(JObject rawJson)
        {
            if (rawJson["id"] != null)
                this.id = int.Parse(rawJson["id"].ToString());
            if (rawJson["title"] != null)
                this.title = rawJson["title"].ToString();
            if (rawJson["points"] != null)
                this.points = int.Parse(rawJson["points"].ToString());
            if (rawJson["description"] != null)
                this.description = rawJson["description"].ToString();
            if (rawJson["reward"] != null)
                this.reward = rawJson["reward"].ToString();
            if (rawJson["rewardItems"] != null && rawJson["rewardItems"].HasValues)
            {
                this.rewardItems = new List<Item>();
                foreach (JObject JRewardItem in rawJson["rewardItems"])
                {
                    Item rewardItem = new Item(JRewardItem);
                    this.rewardItems.Add(rewardItem);
                }
            }
            if (rawJson["icon"] != null)
                this.icon = rawJson["icon"].ToString();
            if (rawJson["criteria"] != null && rawJson["criteria"].HasValues)
            {
                this.criteria = new List<Achievement>();
                foreach (JObject JCriteria in rawJson["criteria"])
                {
                    Achievement criteriaAch = new Achievement(JCriteria);
                    this.criteria.Add(criteriaAch);
                }
            }
            if (rawJson["accountWide"] != null)
                this.accountWide = bool.Parse(rawJson["accountWide"].ToString());
            if (rawJson["orderIndex"] != null)
                this.orderIndex = int.Parse(rawJson["orderIndex"].ToString());
            if (rawJson["max"] != null)
                this.max = int.Parse(rawJson["max"].ToString());
        }
    }
}
