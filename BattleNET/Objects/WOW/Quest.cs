﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Quest
    {
        public int id { get; internal set; }
        public string title { get; internal set; }
        public int reqLevel { get; internal set; }
        public int suggestedPartyMembers { get; internal set; }
        public string category { get; internal set; }
        public int level { get; internal set; }

        public Quest(JObject rawJson)
        {
            this.id = int.Parse(rawJson["id"].ToString());
            this.title = rawJson["title"].ToString();
            this.reqLevel = int.Parse(rawJson["reqLevel"].ToString());
            this.suggestedPartyMembers = int.Parse(rawJson["suggestedPartyMembers"].ToString());
            this.category = rawJson["category"].ToString();
            this.level = int.Parse(rawJson["level"].ToString());
        }
    }
}
