﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Item
    {
        public class ItemStat
        {
            public int stat { get; internal set; }
            public int amount { get; internal set; }

            public ItemStat(JToken JStat)
            {
                if (JStat["stat"] != null)
                    this.stat = int.Parse(JStat["stat"].ToString());
                if (JStat["amount"] != null)
                    this.amount = int.Parse(JStat["amount"].ToString());
            }
        }
        public class ItemItemSpell
        {
            public class ItemItemSpellSpell
            {
                public int id { get; internal set; }
                public string name { get; internal set; }
                public string icon { get; internal set; }
                public string description { get; internal set; }
                public string castTime { get; internal set; }

                public ItemItemSpellSpell(JToken JItemSpellSpell)
                {
                    if (JItemSpellSpell["id"] != null)
                        this.id = int.Parse(JItemSpellSpell["id"].ToString());
                    if (JItemSpellSpell["name"] != null)
                        this.name = JItemSpellSpell["name"].ToString();
                    if (JItemSpellSpell["icon"] != null)
                        this.icon = JItemSpellSpell["icon"].ToString();
                    if (JItemSpellSpell["description"] != null)
                        this.description = JItemSpellSpell["description"].ToString();
                    if (JItemSpellSpell["castTime"] != null)
                        this.castTime = JItemSpellSpell["castTime"].ToString();
                }
            }
            public int spellId { get; internal set; }
            public ItemItemSpellSpell spell { get; internal set; }
            public int nCharges { get; internal set; }
            public bool consumable { get; internal set; }
            public int categoryId { get; internal set; }
            public string trigger { get; internal set; }

            public ItemItemSpell(JToken JItemSpell)
            {
                if (JItemSpell["spellId"] != null)
                    this.spellId = int.Parse(JItemSpell["spellId"].ToString());
                if (JItemSpell["spell"] != null)
                    this.spell = new ItemItemSpellSpell(JItemSpell["spell"]);
                if (JItemSpell["nCharges"] != null)
                    this.nCharges = int.Parse(JItemSpell["nCharges"].ToString());
                if (JItemSpell["consumable"] != null)
                    this.consumable = bool.Parse(JItemSpell["consumable"].ToString());
                if (JItemSpell["categoryId"] != null)
                    this.categoryId = int.Parse(JItemSpell["categoryId"].ToString());
                if (JItemSpell["trigger"] != null)
                    this.trigger = JItemSpell["trigger"].ToString();
            }
        }
        public class ItemWeaponInfo
        {
            public class ItemWeaponInfoDamage
            {
                public int min { get; internal set; }
                public int max { get; internal set; }
                public double exactMin { get; internal set; }
                public double exactMax { get; internal set; }

                public ItemWeaponInfoDamage(JToken JWeaponInfoDamage)
                {
                    this.min = int.Parse(JWeaponInfoDamage["min"].ToString());
                    this.max = int.Parse(JWeaponInfoDamage["max"].ToString());
                    this.exactMin = double.Parse(JWeaponInfoDamage["exactMin"].ToString());
                    this.exactMax = double.Parse(JWeaponInfoDamage["exactMax"].ToString());
                }
            }

            public ItemWeaponInfoDamage damage { get; internal set; }
            public double weaponSpeed { get; internal set; }
            public float dps { get; internal set; }

            public ItemWeaponInfo(JToken JWeaponInfo)
            {
                if (JWeaponInfo["damage"] != null)
                    this.damage = new ItemWeaponInfoDamage(JWeaponInfo["damage"]);
                if (JWeaponInfo["weaponSpeed"] != null)
                    this.weaponSpeed = double.Parse(JWeaponInfo["weaponSpeed"].ToString());
                if (JWeaponInfo["dps"] != null)
                    this.dps = float.Parse(JWeaponInfo["dps"].ToString());
            }
        }
        public class ItemItemSource
        {
            public int sourceId { get; internal set; }
            public string sourceType { get; internal set; }

            public ItemItemSource(JToken JItemSource)
            {
                if (JItemSource["sourceId"] != null)
                    this.sourceId = int.Parse(JItemSource["sourceId"].ToString());
                if (JItemSource["sourceType"] != null)
                    this.sourceType = JItemSource["sourceType"].ToString();
            }
        }
        public class ItemBonusSummary
        {
            public class ItemBonusSummaryBonusChance {
                public string chanceType { get; internal set; }
                public List<ItemStat> stats { get; internal set; }
                public object sockets { get; internal set; }

                public ItemBonusSummaryBonusChance(JToken JBonusSummaryBonusChance)
                {
                    if (JBonusSummaryBonusChance["chanceType"] != null)
                        this.chanceType = JBonusSummaryBonusChance["chanceType"].ToString();
                    if (JBonusSummaryBonusChance["stats"] != null && JBonusSummaryBonusChance["stats"].HasValues)
                    {
                        this.stats = new List<ItemStat>();
                        foreach (JToken JStat in JBonusSummaryBonusChance["stats"])
                        {
                            ItemStat stat = new ItemStat(JStat);
                            this.stats.Add(stat);
                        }
                    }
                    if (JBonusSummaryBonusChance["sockets"] != null)
                        this.sockets = JBonusSummaryBonusChance["sockets"];
                }
            }
            public List<int> defaultBonusLists { get; internal set; }
            public List<int> chanceBonusLists { get; internal set; }
            public List<ItemBonusSummaryBonusChance> bonusChances { get; internal set; }

            public ItemBonusSummary(JToken JBonusSummary)
            {
                if (JBonusSummary["defaultBonusLists"] != null && JBonusSummary["defaultBonusLists"].HasValues)
                {
                    this.defaultBonusLists = new List<int>();
                    foreach (int JDefaultBonusList in JBonusSummary["defaultBonusLists"])
                    {
                        this.defaultBonusLists.Add(JDefaultBonusList);
                    }
                }
                if (JBonusSummary["chanceBonusLists"] != null && JBonusSummary["chanceBonusLists"].HasValues)
                {
                    this.chanceBonusLists = new List<int>();
                    foreach (int JChanceBonusList in JBonusSummary["chanceBonusLists"])
                    {
                        this.chanceBonusLists.Add(JChanceBonusList);
                    }
                }
                if (JBonusSummary["bonusChances"] != null && JBonusSummary["bonusChances"].HasValues)
                {
                    this.bonusChances = new List<ItemBonusSummaryBonusChance>();
                    foreach (JToken JBonusChance in JBonusSummary["bonusChances"])
                    {
                        ItemBonusSummaryBonusChance bonusChance = new ItemBonusSummaryBonusChance(JBonusChance);
                        this.bonusChances.Add(bonusChance);
                    }
                }
            }
        }
        public class ItemTooltipParams
        {
            public class ItemTooltipParamsUpgrade
            {
                public int current { get; internal set; }
                public int total { get; internal set; }
                public int itemLevelIncrement { get; internal set; }

                public ItemTooltipParamsUpgrade(JToken JTooltipParamsUpgrade)
                {
                    if (JTooltipParamsUpgrade["current"] != null)
                        this.current = int.Parse(JTooltipParamsUpgrade["current"].ToString());
                    if (JTooltipParamsUpgrade["total"] != null)
                        this.total = int.Parse(JTooltipParamsUpgrade["total"].ToString());
                    if (JTooltipParamsUpgrade["itemLevelIncrement"] != null)
                        this.itemLevelIncrement = int.Parse(JTooltipParamsUpgrade["itemLevelIncrement"].ToString());
                }
            }

            public ItemTooltipParamsUpgrade upgrade { get; internal set; }
            public int timewalkerLevel { get; internal set; }

            public ItemTooltipParams(JToken JTooltipParams)
            {
                if (JTooltipParams["upgrade"] != null)
                    this.upgrade = new ItemTooltipParamsUpgrade(JTooltipParams["upgrade"]);
                if (JTooltipParams["timewalkerLevel"] != null)
                    this.timewalkerLevel = int.Parse(JTooltipParams["timewalkerLevel"].ToString());
            }
        }

        public int id { get; internal set; }
        public int disenchantingSkillRank { get; internal set; }
        public string description { get; internal set; }
        public string name { get; internal set; }
        public string icon { get; internal set; }
        public int stackable { get; internal set; }
        public int itemBind { get; internal set; }
        public List<ItemStat> bonusStats { get; internal set; }
        public List<ItemItemSpell> itemSpells { get; internal set; }
        public int buyPrice { get; internal set; }
        public int itemClass { get; internal set; }
        public int itemSubClass { get; internal set; }
        public int containerSlots { get; internal set; }
        public ItemWeaponInfo weaponInfo { get; internal set; }
        public int inventoryType { get; internal set; }
        public bool equippable { get; internal set; }
        public int itemLevel { get; internal set; }
        public int maxCount { get; internal set; }
        public int maxDurability { get; internal set; }
        public int minFactionId { get; internal set; }
        public int minReputation { get; internal set; }
        public int quality { get; internal set; }
        public int sellPrice { get; internal set; }
        public int requiredSkill { get; internal set; }
        public int requiredLevel { get; internal set; }
        public int requiredSkillLevel { get; internal set; }
        public ItemItemSource itemSource { get; internal set; }
        public int baseArmor { get; internal set; }
        public bool hasSockets { get; internal set; }
        public bool isAuctionable { get; internal set; }
        public int armor { get; internal set; }
        public int displayInfoId { get; internal set; }
        public string nameDescription { get; internal set; }
        public string nameDescriptionColor { get; internal set; }
        public bool upgradable { get; internal set; }
        public bool heroicTooltip { get; internal set; }
        public string context { get; internal set; }
        public List<int> bonusList { get; internal set; }
        public List<string> availableContexts { get; internal set; }
        public ItemBonusSummary bonusSummary { get; internal set; }

        public ItemTooltipParams tooltipParams { get; internal set; }

        public Item(JObject rawJson)
        {
            if (rawJson["id"] != null)
                this.id = int.Parse(rawJson["id"].ToString());
            if (rawJson["disenchantingSkillRank"] != null)
                this.disenchantingSkillRank = int.Parse(rawJson["disenchantingSkillRank"].ToString());
            if (rawJson["description"] != null)
                this.description = rawJson["description"].ToString();
            if (rawJson["name"] != null)
                this.name = rawJson["name"].ToString();
            if (rawJson["icon"] != null)
                this.icon = rawJson["icon"].ToString();
            if (rawJson["stackable"] != null)
                this.stackable = int.Parse(rawJson["stackable"].ToString());
            if (rawJson["itemBind"] != null)
                this.itemBind = int.Parse(rawJson["itemBind"].ToString());
            if (rawJson["bonusStats"] != null && rawJson["bonusStats"].HasValues)
            {
                this.bonusStats = new List<ItemStat>();
                foreach (JToken JBonusStat in rawJson["bonusStats"])
                {
                    ItemStat bonusStat = new ItemStat(JBonusStat);
                    bonusStats.Add(bonusStat);
                }
            }
            if (rawJson["itemSpells"] != null && rawJson["itemSpells"].HasValues)
            {
                this.itemSpells = new List<ItemItemSpell>();
                foreach (JToken JItemSpell in rawJson["itemSpells"])
                {
                    ItemItemSpell itemSpell = new ItemItemSpell(JItemSpell);
                    itemSpells.Add(itemSpell);
                }
            }
            if (rawJson["buyPrice"] != null)
                this.buyPrice = int.Parse(rawJson["buyPrice"].ToString());
            if (rawJson["itemClass"] != null)
                this.itemClass = int.Parse(rawJson["itemClass"].ToString());
            if (rawJson["itemSubClass"] != null)
                this.itemSubClass = int.Parse(rawJson["itemSubClass"].ToString());
            if (rawJson["containerSlots"] != null)
                this.containerSlots = int.Parse(rawJson["containerSlots"].ToString());
            if (rawJson["weaponInfo"] != null)
                this.weaponInfo = new ItemWeaponInfo(rawJson["weaponInfo"]);
            if (rawJson["inventoryType"] != null)
                this.inventoryType = int.Parse(rawJson["inventoryType"].ToString());
            if (rawJson["equippable"] != null)
                this.equippable = bool.Parse(rawJson["equippable"].ToString());
            if (rawJson["itemLevel"] != null)
                this.itemLevel = int.Parse(rawJson["itemLevel"].ToString());
            if (rawJson["maxCount"] != null)
                this.maxCount = int.Parse(rawJson["maxCount"].ToString());
            if (rawJson["maxDurability"] != null)
                this.maxDurability = int.Parse(rawJson["maxDurability"].ToString());
            if (rawJson["minFactionId"] != null)
                this.minFactionId = int.Parse(rawJson["minFactionId"].ToString());
            if (rawJson["minReputation"] != null)
                this.minReputation = int.Parse(rawJson["minReputation"].ToString());
            if (rawJson["quality"] != null)
                this.quality = int.Parse(rawJson["quality"].ToString());
            if (rawJson["sellPrice"] != null)
                this.sellPrice = int.Parse(rawJson["sellPrice"].ToString());
            if (rawJson["requiredSkill"] != null)
                this.requiredSkill = int.Parse(rawJson["requiredSkill"].ToString());
            if (rawJson["requiredLevel"] != null)
                this.requiredLevel = int.Parse(rawJson["requiredLevel"].ToString());
            if (rawJson["requiredSkillLevel"] != null)
                this.requiredSkillLevel = int.Parse(rawJson["requiredSkillLevel"].ToString());
            if (rawJson["itemSource"] != null)
                this.itemSource = new ItemItemSource(rawJson["itemSource"]);
            if (rawJson["baseArmor"] != null)
                this.baseArmor = int.Parse(rawJson["baseArmor"].ToString());
            if (rawJson["hasSockets"] != null)
                this.hasSockets = bool.Parse(rawJson["hasSockets"].ToString());
            if (rawJson["isAuctionable"] != null)
                this.isAuctionable = bool.Parse(rawJson["isAuctionable"].ToString());
            if (rawJson["armor"] != null)
                this.armor = int.Parse(rawJson["armor"].ToString());
            if (rawJson["displayInfoId"] != null)
                this.displayInfoId = int.Parse(rawJson["displayInfoId"].ToString());
            if (rawJson["nameDescription"] != null)
                this.nameDescription = rawJson["nameDescription"].ToString();
            if (rawJson["nameDescriptionColor"] != null)
                this.nameDescriptionColor = rawJson["nameDescriptionColor"].ToString();
            if (rawJson["upgradeable"] != null)
                this.upgradable = bool.Parse(rawJson["upgradeable"].ToString());
            if (rawJson["heroicTooltip"] != null)
                this.heroicTooltip = bool.Parse(rawJson["heroicTooltip"].ToString());
            if (rawJson["context"] != null)
                this.context = rawJson["context"].ToString();
            if (rawJson["bonusList"] != null && rawJson["bonusList"].HasValues)
            {
                this.bonusList = new List<int>();
                foreach (int id in rawJson["bonusList"])
                {
                    this.bonusList.Add(id);
                }
            }
            if (rawJson["availableContexts"] != null && rawJson["availableContexts"].HasValues)
            {
                this.availableContexts = new List<string>();
                foreach (string context in rawJson["availableContexts"])
                {
                    this.availableContexts.Add(context);
                }
            }
            if (rawJson["bonusSummary"] != null)
                this.bonusSummary = new ItemBonusSummary(rawJson["bonusSummary"]);
            if (rawJson["tooltipParams"] != null)
                this.tooltipParams = new ItemTooltipParams(rawJson["tooltipParams"]);
        }
    }
}
