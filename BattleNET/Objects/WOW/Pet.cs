﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Pet
    {
        public class PetStats
        {
            public int speciesId { get; internal set; }
            public int breedId { get; internal set; }
            public int petQualityId { get; internal set; }
            public int level { get; internal set; }
            public int health { get; internal set; }
            public int power { get; internal set; }
            public int speed { get; internal set; }

            public PetStats(JObject JStats)
            {
                if (JStats["speciesId"] != null)
                    this.speciesId = int.Parse(JStats["speciesId"].ToString());
                if (JStats["breedId"] != null)
                    this.breedId = int.Parse(JStats["breedId"].ToString());
                if (JStats["petQualityId"] != null)
                    this.petQualityId = int.Parse(JStats["petQualityId"].ToString());
                if (JStats["level"] != null)
                    this.level = int.Parse(JStats["level"].ToString());
                if (JStats["health"] != null)
                    this.health = int.Parse(JStats["health"].ToString());
                if (JStats["power"] != null)
                    this.power = int.Parse(JStats["power"].ToString());
                if (JStats["speed"] != null)
                    this.speed = int.Parse(JStats["speed"].ToString());
            }
        }
        public class PetAbility
        {
            public int slot { get; internal set; }
            public int order { get; internal set; }
            public int requiredLevel { get; internal set; }
            public int id { get; internal set; }
            public string name { get; internal set; }
            public string icon { get; internal set; }
            public int cooldown { get; internal set; }
            public int rounds { get; internal set; }
            public int petTypeId { get; internal set; }
            public bool isPassive { get; internal set; }
            public bool hideHints { get; internal set; }

            public PetAbility(JObject JAbility)
            {
                if (JAbility["slot"] != null)
                    this.slot = int.Parse(JAbility["slot"].ToString());
                if (JAbility["order"] != null)
                    this.order = int.Parse(JAbility["order"].ToString());
                if (JAbility["requiredLevel"] != null)
                    this.requiredLevel = int.Parse(JAbility["requiredLevel"].ToString());
                if (JAbility["id"] != null)
                    this.id = int.Parse(JAbility["id"].ToString());
                if (JAbility["name"] != null)
                    this.name = JAbility["name"].ToString();
                if (JAbility["icon"] != null)
                    this.icon = JAbility["icon"].ToString();
                if (JAbility["cooldown"] != null)
                    this.cooldown = int.Parse(JAbility["cooldown"].ToString());
                if (JAbility["rounds"] != null)
                    this.rounds = int.Parse(JAbility["rounds"].ToString());
                if (JAbility["petTypeId"] != null)
                    this.petTypeId = int.Parse(JAbility["petTypeId"].ToString());
                if (JAbility["isPassive"] != null)
                    this.isPassive = bool.Parse(JAbility["isPassive"].ToString());
                if (JAbility["hideHints"] != null)
                    this.hideHints = bool.Parse(JAbility["hideHints"].ToString());
            }
        }
        public class PetSpecies
        {
            public int speciesId { get; internal set; }
            public int petTypeId { get; internal set; }
            public int creatureId { get; internal set; }
            public string name { get; internal set; }
            public bool canBattle { get; internal set; }
            public string icon { get; internal set; }
            public string description { get; internal set; }
            public string source { get; internal set; }
            public List<PetAbility> abilities { get; internal set; }

            public PetSpecies(JObject JSpecies)
            {
                if (JSpecies["speciesId"] != null)
                    this.speciesId = int.Parse(JSpecies["speciesId"].ToString());
                if (JSpecies["petTypeId"] != null)
                    this.petTypeId = int.Parse(JSpecies["petTypeId"].ToString());
                if (JSpecies["creatureId"] != null)
                    this.creatureId = int.Parse(JSpecies["creatureId"].ToString());
                if (JSpecies["name"] != null)
                    this.name = JSpecies["name"].ToString();
                if (JSpecies["canBattle"] != null)
                    this.canBattle = bool.Parse(JSpecies["canBattle"].ToString());
                if (JSpecies["icon"] != null)
                    this.icon = JSpecies["icon"].ToString();
                if (JSpecies["description"] != null)
                    this.description = JSpecies["description"].ToString();
                if (JSpecies["source"] != null)
                    this.source = JSpecies["source"].ToString();
                if (JSpecies["abilities"] != null && JSpecies["abilities"].HasValues)
                {
                    this.abilities = new List<PetAbility>();
                    foreach (JObject JAbility in JSpecies["abilities"])
                    {
                        this.abilities.Add(new PetAbility(JAbility));
                    }
                }
            }
        }

        public bool canBattle { get; internal set; }
        public int creatureId { get; internal set; }
        public string name { get; internal set; }
        public string family { get; internal set; }
        public string icon { get; internal set; }
        public int qualityId { get; internal set; }
        public PetStats stats { get; internal set; }
        public List<string> strongAgainst { get; internal set; }
        public int typeId { get; internal set; }
        public List<string> weakAgainst { get; internal set; }

        public Pet(JObject rawJson)
        {
            if (rawJson["canBattle"] != null)
                this.canBattle = bool.Parse(rawJson["canBattle"].ToString());
            if (rawJson["creatureId"] != null)
                this.creatureId = int.Parse(rawJson["creatureId"].ToString());
            if (rawJson["name"] != null)
                this.name = rawJson["name"].ToString();
            if (rawJson["family"] != null)
                this.family = rawJson["family"].ToString();
            if (rawJson["icon"] != null)
                this.icon = rawJson["icon"].ToString();
            if (rawJson["qualityId"] != null)
                this.qualityId = int.Parse(rawJson["qualityId"].ToString());
            if (rawJson["stats"] != null)
                this.stats = new PetStats(JObject.Parse(rawJson["stats"].ToString()));
            if (rawJson["strongAgainst"] != null && rawJson["strongAgainst"].HasValues)
            {
                this.strongAgainst = new List<string>();
                foreach (string JStrongAgainst in rawJson["strongAgainst"])
                {
                    this.strongAgainst.Add(JStrongAgainst);
                }
            }
            if (rawJson["typeId"] != null)
                this.typeId = int.Parse(rawJson["typeId"].ToString());
            if (rawJson["weakAgainst"] != null && rawJson["weakAgainst"].HasValues)
            {
                this.weakAgainst = new List<string>();
                foreach (string JWeakAgainst in rawJson["weakAgainst"])
                {
                    this.weakAgainst.Add(JWeakAgainst);
                }
            }
        }
    }
}
